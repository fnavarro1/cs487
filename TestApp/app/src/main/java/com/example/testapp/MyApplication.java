package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

//contains global variables
public class MyApplication extends Application {

    static ArrayList<Order> orders = new ArrayList<Order>();
    //used to globally store orders that are not submitted yet
    static ArrayList<Order> tempOrders = new ArrayList<Order>();
    //used to check if MakeOrder should create a new temp order
    static boolean isNewOrder = true;
    static ArrayList<UserAccount> accounts = new ArrayList<>();
    static UserAccount currentAcc;

    public static ArrayList<Order> getOrders()
    {
        return orders;
    }

    public static void addOrder(Order newOrder)
    {
        orders.add(newOrder);
    }

    public static void addTempOrder(Order o){tempOrders.add(o);}

    public static Order getTempOrder(int index){
        return tempOrders.get(index);
    }

    public static void removeOrder(Order o){orders.remove(o);}

    public static void removeTempOrder(Order o){tempOrders.remove(o);}

    public static void resetOrderList()
    {
        orders.clear();
    }

    public static void createAccount(String un, String pw){
        accounts.add(new UserAccount(un,pw,0));
    }

    public static boolean userLogin(String un, String pw){
        for(UserAccount a:accounts){
            if(a.verify(un,pw)){
                currentAcc = a;
                return true;
            }
        }
        return false;
    }

    public static void userLoginAsGuest(){
        currentAcc = new UserAccountGuest();
    }

    public static String getCurrentUsername(){
        return currentAcc.getUsername();
    }

    public static int getPoints(){return currentAcc.getPoints();}

    public static void addPointsToAccount(int points)
    {
         currentAcc.addPoints(points);
    }

    public static void changePassword(String pw){
        currentAcc.changePassword(pw);
    }

    public static boolean isNewOrder(){
        return isNewOrder;
    }

    public static void  setIsNewOrder(boolean b){
        isNewOrder = b;
    }

    public static int redeemPoints(int cost) { return currentAcc.redeemPoints(cost); }

    public static int getCostIfRedeemPoints(int cost) {return currentAcc.getCostIfRedeemPoints(cost); }

    public static boolean isGuest() { return currentAcc.isGuest(); }
}