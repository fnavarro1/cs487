package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class StaffHome extends AppCompatActivity {

    //String orderNames[], orderProgress[];

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_home);

        ArrayList<Order> orders = MyApplication.getOrders();


        recyclerView = findViewById(R.id.orders_list_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        StaffAdapter staffAdapter = new StaffAdapter(this, orders);
        recyclerView.setAdapter(staffAdapter);


    }
}
