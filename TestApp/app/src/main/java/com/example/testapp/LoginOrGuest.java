package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginOrGuest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_or_guest);

        Button goLogin = (Button) findViewById(R.id.goToLogin);
        goLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextUsername = (EditText) findViewById((R.id.login_username_edit_text));
                EditText editTextPassword = (EditText) findViewById((R.id.login_password_edit_text));
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();

                if(MyApplication.userLogin(username,password)){
                    //do the sign in


                    Intent loginIntent = new Intent(getApplicationContext(), CustomerHome.class);
                    startActivity(loginIntent);
                }
                else{
                    TextView fuck = (TextView) findViewById(R.id.login_failed_text);
                    fuck.setText("Username/Password combination not found");
                }

            }
        });

        Button goGuest = (Button) findViewById(R.id.goToGuest);
        goGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.userLoginAsGuest();
                Intent loginIntent = new Intent(getApplicationContext(), CustomerHome.class);
                startActivity(loginIntent);
            }
        });

        Button newAccount = (Button) findViewById(R.id.new_account_button);
        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent createAccount = new Intent(getApplicationContext(), CreateAccount.class);
                startActivity(createAccount);
            }
        });
    }
}