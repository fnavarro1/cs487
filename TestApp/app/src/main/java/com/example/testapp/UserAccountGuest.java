package com.example.testapp;

import java.util.ArrayList;

public class UserAccountGuest extends UserAccount {

    public UserAccountGuest(){
        super();
    }

    public boolean isGuest()
    {
        return true;
    }

    public boolean verify(String un, String pw){
        return false;
    }

    public String getUsername(){
        return "";
    }

    public void addOrderToHistory(Order o){}

    public void changePassword(String pw){}

    public ArrayList<Order> getOrderHistory(){
        return null;
    }

    public void addPoints(int p){}

    public int redeemPoints(int cost) {
        return cost;
    }

    public int getCostIfRedeemPoints(int cost) {
        return cost;
    }

    public int getPoints(){return 0;}
}
