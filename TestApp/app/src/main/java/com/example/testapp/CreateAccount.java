package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateAccount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        Button create = (Button) findViewById(R.id.create_account_button);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextUsername = (EditText) findViewById((R.id.create_username_edit_text));
                String username = editTextUsername.getText().toString();

                EditText editTextPassword = (EditText) findViewById((R.id.create_password_edit_text));
                String password = editTextPassword.getText().toString();

                MyApplication.createAccount(username, password);
                Intent loginIntent = new Intent(getApplicationContext(), LoginOrGuest.class);
                startActivity(loginIntent);
                finish();
            }
        });
    }
}