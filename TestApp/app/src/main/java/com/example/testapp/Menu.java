package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class Menu extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // menu buttons
        ImageButton goAddItem1 = (ImageButton) findViewById(R.id.goAddItem1); //make a new button
        goAddItem1.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                String itemName = "Chicken Wings";
                Intent addItemIntent = new Intent(getApplicationContext(), MakeOrder.class);
                addItemIntent.putExtra("itemName", (CharSequence) itemName);
                addItemIntent.putExtra("itemWaitTime", 3);
                addItemIntent.putExtra("itemCost", 7);
                startActivity(addItemIntent);
            }
        });

        ImageButton goAddItem2 = (ImageButton) findViewById(R.id.goAddItem2); //make a new button
        goAddItem2.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                TextView itemList = (TextView) findViewById(R.id.itemList);
                String itemName = "Chicken Rice";
                Intent addItemIntent2 = new Intent(getApplicationContext(), MakeOrder.class);
                addItemIntent2.putExtra("itemName", (CharSequence) itemName);
                addItemIntent2.putExtra("itemWaitTime", 7);
                addItemIntent2.putExtra("itemCost", 9);
                startActivity(addItemIntent2);
            }
        });

        ImageButton goAddItem3 = (ImageButton) findViewById(R.id.goAddItem3); //make a new button
        goAddItem3.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                String itemName = "Chicken Soup";
                Intent addItemIntent3 = new Intent(getApplicationContext(), MakeOrder.class);
                addItemIntent3.putExtra("itemName", (CharSequence) itemName);
                addItemIntent3.putExtra("itemWaitTime", 6);
                addItemIntent3.putExtra("itemCost", 4);
                startActivity(addItemIntent3);
            }
        });

        ImageButton goAddItem4 = (ImageButton) findViewById(R.id.goAddItem4); //make a new button
        goAddItem4.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                String itemName = "Chicken Pizza";
                Intent addItemIntent4 = new Intent(getApplicationContext(), MakeOrder.class);
                addItemIntent4.putExtra("itemName", (CharSequence) itemName);
                addItemIntent4.putExtra("itemWaitTime", 15);
                addItemIntent4.putExtra("itemCost", 10);
                startActivity(addItemIntent4);
            }
        });

        ImageButton goAddItem5 = (ImageButton) findViewById(R.id.goAddItem5); //make a new button
        goAddItem5.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                String itemName = "Chicken Taco";
                Intent addItemIntent5 = new Intent(getApplicationContext(), MakeOrder.class);
                addItemIntent5.putExtra("itemName", (CharSequence) itemName);
                addItemIntent5.putExtra("itemWaitTime", 3);
                addItemIntent5.putExtra("itemCost", 8);
                startActivity(addItemIntent5);
            }
        });

        ImageButton goAddItem6 = (ImageButton) findViewById(R.id.goAddItem6); //make a new button
        goAddItem6.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                String itemName = "Lemonade";
                Intent addItemIntent6 = new Intent(getApplicationContext(), MakeOrder.class);
                addItemIntent6.putExtra("itemName", (CharSequence) itemName);
                addItemIntent6.putExtra("itemWaitTime", 1);
                addItemIntent6.putExtra("itemCost", 2);
                startActivity(addItemIntent6);
            }
        });
    }
}