package com.example.testapp;

import android.app.Application;

import java.util.ArrayList;

public class PointManager extends Application {

    ArrayList<Integer> pointArray = new ArrayList<>(); //each one is a seperate account

    void addNewAccount()
    {
        pointArray.add(0);
    }

    boolean addPointsToAccount(int i, int points)
    {
        if (points < 0)
            return false;
        pointArray.set(i, pointArray.get(i).intValue() + points);
        return true;
    }

    boolean deductPointsFromAccount(int i, int points)
    {
        if (points < 0)
            return false;
        if (pointArray.get(i).intValue() < points)
            return false;
        pointArray.set(i, pointArray.get(i).intValue() - points);
        return true;
    }
}
