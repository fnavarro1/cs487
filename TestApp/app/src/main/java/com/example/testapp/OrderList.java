package com.example.testapp;

import android.app.Application;

import java.util.ArrayList;

public class OrderList extends Application {

    static ArrayList<Order> orders = new ArrayList<Order>();

    public static ArrayList<Order> getOrders()
    {
        return orders;
    }

    public void addOrder(Order newOrder)
    {
        orders.add(newOrder);
    }

    public void removeOrder(Order o){
        orders.remove(o);
    }

    public void resetOrderList()
    {
        orders.clear();
    }
}