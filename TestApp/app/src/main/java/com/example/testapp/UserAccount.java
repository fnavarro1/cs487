package com.example.testapp;

import java.util.ArrayList;

public class UserAccount {

    String username, password;
    ArrayList<Order> orderHistory = new ArrayList<>();
    int points;

    public UserAccount(String un, String pw, int p){
        username = un;
        password = pw;
        points = p;
    }

    protected UserAccount(){}

    public boolean verify(String un, String pw){

        return (pw.equals(password)  && un.equals(username));
    }

    public String getUsername(){
        return username;
    }

    public void addOrderToHistory(Order o){
        orderHistory.add(o);
    }

    public void changePassword(String pw){
        password = pw;
    }

    public ArrayList<Order> getOrderHistory(){
        return orderHistory;
    }

    public void addPoints(int p){
        points += p;
    }

    public int redeemPoints(int cost) {
        if (points >= cost) {
            points -= cost;
            return 0;}
        else {
            cost -= points;
            points = 0;
            return cost;
        }
    }

    public int getCostIfRedeemPoints(int cost) {
        if (points >= cost) {
            return 0;}
        else {
            cost -= points;
            return cost;
        }
    }

    public int getPoints(){return points;}

    public boolean isGuest()
    {
        return false;
    }
}
