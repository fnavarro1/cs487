package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MakeOrder extends AppCompatActivity {

    //CharSequence lastTime;

    //Doesn't work
//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        super.onSaveInstanceState(savedInstanceState);
//        TextView itemList = (TextView) findViewById(R.id.itemList);
//        savedInstanceState.putCharSequence("itemList", itemList.getText());
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_order);

        //CharSequence lastTime = savedInstanceState.getString("itemList");

        Button goToMenu = (Button) findViewById(R.id.goToMenu);
        goToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menuIntent = new Intent(getApplicationContext(), Menu.class);
                startActivity(menuIntent);
            }
        });

        Order newOrder;

        if(MyApplication.isNewOrder()) {
            //temp version of order, added to global list when order is complete
            newOrder = new Order();
            MyApplication.addTempOrder(newOrder);
            MyApplication.setIsNewOrder(false);
        }
        else{
            //hardcoding this is hacky and bad but it will work for now
            newOrder = MyApplication.getTempOrder(0);
        }

        if (getIntent().hasExtra("itemName")) {
            TextView itemList = (TextView) findViewById(R.id.itemList);
            TextView emptyCart = (TextView) findViewById(R.id.emptyCart);
            String text = getIntent().getExtras().getString("itemName");
            //changed this to account for order list
            newOrder.addItem(text);
            text = newOrder.getItems();
            //end of changes

            itemList.setText(text);
            //itemList.setText(itemList.getText() + text);
            emptyCart.setText("");
        }

        if (getIntent().hasExtra("itemWaitTime")) {
            TextView totalWaitTime = (TextView) findViewById(R.id.totalWaitTime);
            int waitTime = getIntent().getExtras().getInt("itemWaitTime");
            //changed this to account for order list
            newOrder.addToWaitTime(waitTime);
            waitTime = newOrder.getWaitTime();
            //end of changes
            totalWaitTime.setText("Wait Time:  " + waitTime + " Min");

        }

        if (getIntent().hasExtra("itemCost")) {
            TextView totalCost = (TextView) findViewById(R.id.totalCost);
            int cost = getIntent().getExtras().getInt("itemCost");
            //changed this to account for order list
            newOrder.addToCost(cost);
            cost = newOrder.getCosts();
            //end of changes
            totalCost.setText("Total:  $" + cost + ".00");
        }

        Button goPlaceOrder = (Button) findViewById(R.id.goPlaceOrder);
        goPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent checkout = new Intent(getApplicationContext(), Checkout.class);
                startActivity(checkout);
            }
        });

    }
}