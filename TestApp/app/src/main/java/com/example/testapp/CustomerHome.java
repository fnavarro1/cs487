package com.example.testapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CustomerHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        ///////////////////STUFF I MADE///////////////////////////////////////////

        TextView userName = (TextView) findViewById(R.id.out_username);
        if (MyApplication.isGuest())
            userName.setText("Guest");
        else
            userName.setText(MyApplication.getCurrentUsername());

        Button goToAccount = (Button) findViewById(R.id.goToAccount); //make a new button
        goToAccount.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                if (MyApplication.isGuest()) {
                    TextView fuck = (TextView) findViewById(R.id.guest_tried_account);
                    fuck.setText("You are logged in as Guest");
                }
                else {
                    Intent accountIntent = new Intent(getApplicationContext(), Account.class); //create an intent, apparently android will interpret it
                    startActivity(accountIntent);
                }
            }
        });

        Button goToMakeOrder = (Button) findViewById(R.id.goToMakeOrder);
        goToMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent makeOrderIntent = new Intent(getApplicationContext(), MakeOrder.class);
                startActivity(makeOrderIntent);
            }
        });
        /////////////////Stuff I made ends here////////////////////////////////////////

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}