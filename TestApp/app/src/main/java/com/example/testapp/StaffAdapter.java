package com.example.testapp;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;

import java.util.ArrayList;


public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.MyViewHolder>{

    //data types here
    ArrayList<Order> orders = new ArrayList<Order>();
    Context context;

    //have to change constructor based on data types
    public StaffAdapter(Context ct, ArrayList<Order> o){
        orders = o;
        context = ct;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.orders_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.orderText.setText(orders.get(position).getItems());
        //may have to change locale
        holder.progressText.setText(String.format("%d",orders.get(position).getWaitTime()));

        holder.complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orders.remove(position);
                notifyDataSetChanged();
            }
        });
    }



    @Override
    public int getItemCount() {
        return orders.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView orderText, progressText;
        Button complete;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            orderText = itemView.findViewById(R.id.order_text);
            progressText = itemView.findViewById(R.id.progress_text);
            complete = itemView.findViewById(R.id.complete_order_button);
        }

    }
}
