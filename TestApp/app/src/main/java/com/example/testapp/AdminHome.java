package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdminHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);

        //       buttons
        Button goToAccManagementButton = (Button) findViewById(R.id.goToAccManagementButton); //make a new button
        goToAccManagementButton.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                Intent i = new Intent(getApplicationContext(), AccountManagement.class);
                startActivity(i);
            }
        });

        Button goToMenuEditButton = (Button) findViewById(R.id.goToMenuEditButton);
        goToMenuEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), EditMenu.class);
                startActivity(i);
            }
        });

        Button goToCustomerDataButton = (Button) findViewById(R.id.goToCustData);
        goToCustomerDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), StaffHome.class);
                startActivity(i);
            }
        });

    }


}