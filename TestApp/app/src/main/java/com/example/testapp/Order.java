package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Order extends Application { //this was called ItemList, changed to Order

    String itemList = "";
    private int totalWaitTime = 0;
    private int totalCost = 0;

    public String getItems()
    {
        return itemList;
    }

    public void addItem(String newItem)
    {
        itemList = itemList + "\n" + newItem;
    }

    public void resetItemList()
    {
        itemList = "";
    }

    public int getWaitTime()
    {
        return totalWaitTime;
    }

    public void addToWaitTime(int newWaitTime)
    {
        totalWaitTime += newWaitTime;
    }

    public int getCosts()
    {
        return totalCost;
    }

    public void addToCost(int newCost)
    {
        totalCost += newCost;
    }
}