package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class Checkout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        //hacky and bad once again
        Order newOrder = MyApplication.getTempOrder(0);

        TextView total = (TextView) findViewById(R.id.checkout_total);
        total.setText(String.format("Total: $%d.00",newOrder.getCosts()));

        Button finish = (Button) findViewById(R.id.finish_and_pay_button);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Switch creditCard = (Switch) findViewById(R.id.creditCard);
                Switch paypal = (Switch) findViewById(R.id.paypal);
                Switch redeemPoints = (Switch) findViewById(R.id.redeemPoints);

                if (creditCard.isChecked() == paypal.isChecked()) {
                    TextView fuck = (TextView) findViewById(R.id.too_many_payment);
                    fuck.setText("Please choose one payment method");
                    return;
                } //only works for two booleans

                MyApplication.addOrder(newOrder);
                if (redeemPoints.isChecked())
                { MyApplication.addPointsToAccount(MyApplication.redeemPoints(newOrder.getCosts())/10); }
                else
                { MyApplication.addPointsToAccount(newOrder.getCosts()/10); }

                //remove from temp list
                MyApplication.removeTempOrder(newOrder);
                MyApplication.setIsNewOrder(true);

                Intent next = new Intent(getApplicationContext(), CustomerHome.class);
//                if (creditCard.isChecked()) { next.putExtra("paymentMethod", "CreditCard"); }
//                else { next.putExtra("paymentMethod", "Paypal"); }
//                next.putExtra("redeemPoints", redeemPoints.isChecked());
                startActivity(next);
                finishAffinity();
            }
        });

        Switch redeemPoints = (Switch) findViewById(R.id.redeemPoints);
        redeemPoints.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TextView total = (TextView) findViewById(R.id.checkout_total);

                if (isChecked)
                {
                    total.setText(String.format("Total: $%d.00",MyApplication.getCostIfRedeemPoints(newOrder.getCosts())));
                }
                else { total.setText(String.format("Total: $%d.00",newOrder.getCosts())); }
            }
        });
    }
}