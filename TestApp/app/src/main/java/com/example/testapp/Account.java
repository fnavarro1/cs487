package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Account extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        String points = String.format("Points: %d", MyApplication.getPoints());
        TextView pointsView = findViewById(R.id.account_points);
        EditText changePW = findViewById(R.id.editTextTextPassword);
        pointsView.setText(points);

        Button shit = findViewById(R.id.account_change_password_button);
        shit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyApplication.changePassword(changePW.getText().toString());
            }
        });
    }
}