package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ItemList extends Application {

    String itemList = "";

    public String getItems()
    {
        return itemList;
    }

    public void addItem(String newItem)
    {
        itemList = itemList + "\n" + newItem;
    }

    public void resetItemList()
    {
        itemList = "";
    }
}