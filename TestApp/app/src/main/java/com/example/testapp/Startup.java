package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Startup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        //       buttons
        Button goToCustomer = (Button) findViewById(R.id.goToCustomer); //make a new button
        goToCustomer.setOnClickListener(new View.OnClickListener() { //give the button a task to do on click
            @Override
            public void onClick(View view) { //the task to do
                Intent customerIntent = new Intent(getApplicationContext(), LoginOrGuest.class);
                startActivity(customerIntent);
            }
        });

        Button goToAdmin = (Button) findViewById(R.id.goToAdmin);
        goToAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent adminIntent = new Intent(getApplicationContext(), AdminHome.class);
                startActivity(adminIntent);
            }
        });

        Button goToStaff = (Button) findViewById(R.id.goToStaff);
        goToStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent staffIntent = new Intent(getApplicationContext(), StaffHome.class);
                startActivity(staffIntent);
            }
        });

    }
}